# Netbox - Onboarding Guide
> Netbox is an open-source Python web application designed to ease the management of IT assets (routers, racks, VM, phones, etc.)
It comes with a series of built-in modules to handle standard items but can also be extended using plugins.
There is an [official Netbox Plugin tutorial](https://github.com/netbox-community/netbox-plugin-tutorial/blob/main/tutorial/step01-initial-setup.md)
> to experience how additional feature can be implemented.
> But it requires to go through a [painful installation process](https://docs.netbox.dev/en/stable/installation/), involving the local deployment of databases (PostgreSQL and Redis).
> Instead, this repo provides and extra framework around the official tutorial to ease the onboarding, thanks to submodules and docker deployment of dependencies.

## Understand the architecture of Netbox
Netbox core is a Django (Python) web application (API+GUI).
It has a series of dependencies, including default plugins that are always active (example: the module for IP address management.)
Those dependencies are listed in the Netbox core [`requirement.txt`](netbox-core/requirements.txt) and must be installed before any execution.
```shell
# Example of dependencies installation
pip install requirements.txt
# If you have an error while installing django-pglocks do this procedure:
sudo apt-get install libffi-dev
pyenv uninstall 3.11.6
pyenv install 3.11
# And then restart pip install requirements.txt (in the venv previously created)
# Example checkup of installed dependencies
pip list
```

After the installation of mandatory dependencies, Netbox is started thanks to
the execution of a provided (and obscure) python script called "manage.py".
```shell
# Example of manage.py execution, using python
python manage.py runserver
```

Later, additional optional plugins may be developed and integrated. The development looks like the creation of any standalone Python package.
It then must be
* installed within the python (virtual) environment of Netbox core. Or netbox core will not be able to find it
  ```shell
  # Execute the setup script of the custom new plugin
  python setup.py develop
  # And make sure it is listed with the other dependencies / default plugins
  pip list
  ```
* explicitly listed into Netbox core configuration file. Or Netbox core doesn't know it has to be loaded
  ```python
   # edit netbox core configuration.py
   PLUGINS = ["my-new-plugin"]
   ```
* Netbox restarted
  ```shell
  # execute once again
  python manage.py runserver
  ```

## What to do if you get stuck?
If you're stuck while doing the official tutorial, you can compare your code with mine in the ```nbox/netbox-plugin-demo-solution``` directory of this repository.

## Instructions
### Step 0: Pre-requisites
Your lab VM must have been initialized first using [this procedure](https://code.europa.eu/digit-c4/dev/ansible-collection/-/tree/main/example?ref_type=heads#dev-vm-initialization).
And the project is initialized according to [Best practices](https://code.europa.eu/digit-c4/dev/best-practices)
```bash
cd ~/Workspace/code.europa.eu/digit-c4/dev  # Create this path using mkdir -p ~/Workspace/code.europa.eu/digit-c4/dev if it does not exists
git clone https://code.europa.eu/digit-c4/dev/netbox-training-workspace.git
cd netbox-training-workspace
git submodule update --init --recursive
```

The first step is to create a new virtual environment according to [python best practices]((https://code.europa.eu/digit-c4/dev/best-practices))
```bash
python -m venv venv && source venv/bin/activate
```

Finally, comes the time to install and run Netbox application. This is simplified in few lines instead of pages of instructions found in the official install guide
```bash
# Quick start the databases (if this does not work, try replacing 'docker compose' with 'docker-compose')
docker compose up -d && docker ps
# Provide Netbox core with the settings to connec to the local Docker databases
cp configuration.py netbox-core/netbox/netbox/configuration.py
# Install Netbox core Python dependencies
pip install -r netbox-core/requirements.txt
# Run the Netbox core init procedure
python netbox-core/netbox/manage.py migrate
python netbox-core/netbox/manage.py createsuperuser
# Run the Netbox application
python netbox-core/netbox/manage.py runserver 0.0.0.0:8000 --insecure
```

You're now ready to go through the [official Netbox plugin tutorial](https://github.com/netbox-community/netbox-plugin-tutorial/blob/main/tutorial/step01-initial-setup.md).

### Step 0.5: Important notes before going through the official tutorial

* The complex official init procedure will keep the Netbox Python virtual environment in `/opt/netbox/venv`. Activation is therefore documented as follow:
  ```bash
  source /opt/netbox/venv/bin/activate
  ```
  Instead, it must be activated thanks to
  ```bash
  source venv/bin/activate
  ```

* The Netbox install path is the `netbox-core` directory in the project root.
  The Netbox configuration file is therefore located in
  `./netbox-core/netbox/netbox/configuration.py`

* All the plugin paths of the tutorial must be adapted, relative to the `netbox-plugin-demo` folder in the project root. Example: the `netbox_access_lists/__init__.py`
  actually corresponds to `netbox-plugin-demo/netbox_access_lists/__init__.py`
  ```
  -                         [project root]
  | - netbox-core           [netbox install path]
  | - netbox-plugin-demo    [tutorial reference point]
  ```

### Step 1: Initial Setup
The https://github.com/netbox-community/netbox-plugin-demo repo has already been initialized for you, as
git submodule in the ```netbox-training-workspace/netbox-plugin-demo``` directory.
You can therefore directly move there and jump to the [Plugin Configuration](https://github.com/netbox-community/netbox-plugin-tutorial/blob/main/tutorial/step01-initial-setup.md#plugin-configuration) section
```bash
cd netbox-training-workspace/netbox-plugin-demo
# Hide all solutions by checking out this branch
git checkout step00-empty
mkdir netbox_access_lists
touch netbox_access_lists/__init__.py
# Now continue with the official tutorial
```

> Also, you need to **RESTART** the `manage.py runserver` command for the change in the plugin to be interpreted.

### Step 2 to 11:
Just follow the official tutorial. When you want to see your changes in action, refer to step 5.2 of this guide to re-install a newer version of the plugin.

## Troubleshooting
If you get the following error
```sh
pip install -r netbox-core/requirements.txt
 
  File "<frozen importlib._bootstrap>", line 241, in _call_with_frames_removed
        File "/tmp/pip-build-env-phofyupo/overlay/lib/python3.11/site-packages/wheel/bdist_wheel.py", line 27, in <module>
          from .macosx_libfile import calculate_macosx_platform_tag
        File "/tmp/pip-build-env-phofyupo/overlay/lib/python3.11/site-packages/wheel/macosx_libfile.py", line 43, in <module>
          import ctypes
        File "/home/wilmach/.pyenv/versions/3.11.6/lib/python3.11/ctypes/__init__.py", line 8, in <module>
          from _ctypes import Union, Structure, Array
      ModuleNotFoundError: No module named '_ctypes'
      [end of output]
```

Try to do this:
```sh
sudo apt-get install libffi-dev
pyenv uninstall 3.11
pyenv install 3.11
pip install -r netbox-core/requirements.txt
```

## TODO

| :exclamation: TODO         |
|:---------------------------|
| Find a way to include demo data |
